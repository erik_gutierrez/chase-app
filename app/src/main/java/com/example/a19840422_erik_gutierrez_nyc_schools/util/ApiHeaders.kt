package com.example.a19840422_erik_gutierrez_nyc_schools.util

object ApiHeaders {
    const val ACCEPT_JSON = "Accept: application/json"
    const val CONTENT_TYPE_JSON = "Content-Type: application/json"
}