package com.example.a19840422_erik_gutierrez_nyc_schools.util

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableContainer

/**
 * Adds the [Disposable] to a [CompositeDisposable] and returns itself
 */
fun <T> T.disposedBy(disposables: DisposableContainer): T where T : Disposable =
        also { disposables.add(it) }