package com.example.a19840422_erik_gutierrez_nyc_schools.util

/**
 * A stateful wrapper for content. Represents a common Loading, Content, Error (LCE) pattern, and
 * provides easy transformers between states and between types.
 */
sealed class Resource<T>(open val data: T?) {
    /**
     * Indicates a loading state. Content may or may not be available at this time, but in a loading
     * state the [data] is likely to change to a new state soon. Use the [loading] transformer to
     * continue previously available content while loading.
     *
     * @see [loading]
     */
    data class Loading<T>(override val data: T? = null) : Resource<T>(data)

    /**
     * Indicates a state change has successfully occurred and content, if any, is now available.
     * Note that this indicates both complete and partial success outcomes, so the content available
     * in this wrapper may require further validations.
     */
    data class Success<T>(override val data: T?) : Resource<T>(data)

    /**
     * Indicates a failure to load content or that an attempted operation did not succeed. Content
     * may or may not be available. An [error] is required to indicate the cause of the failure.
     *
     * In the case that you want to continue showing the previously available content, you can use
     * [withError] to transform the prior state to a failure state.
     *
     * @see [withError]
     */
    data class Failure<T>(override val data: T? = null, val error: Throwable) : Resource<T>(data)

    companion object {
        /**
         * Get a Resource from a value producer that may throw an exception. The result of the
         * `dataProvider` is wrapped as either a [Resource.Success] with the returned result or
         *  a [Resource.Failure] with the exception thrown. Process the exception further by
         *  providing an [errorProvider]. Provide a default value for [data] with [fallbackData].
         *
         *  @param fallbackData a default [data] value to set if a [Failure] is returned.
         *  Default: `null`
         *  @param errorProvider a lambda providing the exception caught from the [dataProvider]
         *  Default: returns the exception thrown by the [dataProvider].
         *  @param dataProvider a lambda that will produce the desired value or throw an exception.
         *  @return a [Success] wrapping the result of the [dataProvider] if no errors were thrown,
         *  otherwise a [Failure] wrapping the [fallbackData] and result of [errorProvider]
         */
        fun <T> from(
            fallbackData: T? = null,
            errorProvider: ((Throwable) -> Throwable) = { it: Throwable -> it },
            dataProvider: () -> T?
        ): Resource<T> {
            return try {
                Success(dataProvider())
            } catch (error: Throwable) {
                Failure(fallbackData, errorProvider(error))
            }
        }
    }
}

/**
 * Returns the data from the original resource now wrapped in a [Resource.Loading] state.
 *
 * @return a [Resource.Loading] containing the data from the original resource.
 */
fun <T> Resource<T>?.loading(): Resource.Loading<T> {
    return Resource.Loading(this?.data)
}

/**
 * Returns the data from the original resource now wrapped in a [Resource.Success] state.
 *
 * @return a [Resource.Success] containing the data from the original resource.
 */
fun <T> Resource<T>?.success(): Resource.Success<T> {
    return Resource.Success(this?.data)
}

/**
 * Returns the data from the original resource now wrapped in a [Resource.Failure] state with the
 * [error] set.
 *
 * @param error the [Throwable] that caused the failure.
 * @return a [Resource.Failure] with the original data and the cause of the failure.
 */
fun <T> Resource<T>?.withError(error: Throwable): Resource.Failure<T> {
    return Resource.Failure(this?.data, error)
}

/**
 * Returns a new resource of the same type and error (if applicable) as the previous resource, but
 * wrapping a new value of the type returned from the [transform] function.
 *
 * @param transform a mapping operation transforming the data of type [T] to a new data of type [R]
 * @return a [Resource] matching the original, but wrapping the value returned in [transform]
 */
fun <T, R> Resource<T>.map(transform: (T?) -> R?): Resource<R> {
    return when (this) {
        is Resource.Success -> Resource.Success(transform.invoke(data))
        is Resource.Loading -> Resource.Loading(transform.invoke(data))
        is Resource.Failure -> Resource.Failure(transform.invoke(data), error)
    }
}

/**
 * Throws an exception given a certain condition. If the `shouldFailWith` option returns an
 * exception, otherwise returns the resource as-is. Useful in cases where you want to throw an
 * exception, but you don't know the state of your resource. By default, it will force throw if the
 * resource is a `Failure` with an exception. If you pass lambda, you can return the exception
 * you would like to throw from the lambda, or return null if you don't want to throw.
 *
 * Some exceptions should always be considered fatal.
 *
 * Example:
 *
 * ```
 *   // When you want to return a resource, but some cases should terminate the stream.
 *   exampleApi.fetchThing()
 *       .map { it.bodyOrError() }
 *       .map { Resource.Success(it) }
 *       .onErrorReturn { Resource.Failure(it) }
 *       .map { it.forceThrow { error -> error as? JsonDataException } }
 * ```
 *
 *   This example will emit a `Resource.Success`, `Resource.Failure`, or will call `onError()` with
 *   the `JsonDataException`.
 *
 * @param shouldFailWith a lambda providing an exception if it exists, and returning an exception
 *      that should be thrown. Return `null` to return the resource without throwing.
 * @return The resource exactly as it is
 * @throws Throwable exception is returned from the `shouldFailWith` lambda.
 */
fun <T> Resource<T>.forceThrow(shouldFailWith: ((Throwable?) -> Throwable?) = { it }): Resource<T> {
    return when (this) {
        is Resource.Failure -> shouldFailWith(error)?.let<Throwable, Nothing> { throw it }
        else -> shouldFailWith(null)?.let<Throwable, Nothing> { throw it }
    } ?: this
}
