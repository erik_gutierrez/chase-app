package com.example.a19840422_erik_gutierrez_nyc_schools.util

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.Moshi
import com.squareup.moshi.nextAnnotations
import java.lang.reflect.Type

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class SerializedNulls {
    object Factory : JsonAdapter.Factory {
        override fun create(
                type: Type,
                annotations: MutableSet<out Annotation>,
                moshi: Moshi
        ): JsonAdapter<*>? {
            return annotations.nextAnnotations<SerializedNulls>()
                    ?.let { nextAnnotations -> moshi.nextAdapter<Any>(this, type, nextAnnotations) }
                    ?.serializeNulls()
        }
    }
}
