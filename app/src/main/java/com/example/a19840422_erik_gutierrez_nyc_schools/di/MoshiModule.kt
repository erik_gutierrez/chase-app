package com.example.a19840422_erik_gutierrez_nyc_schools.di
import com.squareup.moshi.Moshi

object MoshiModule {
    fun productsMoshi(): Moshi {
        return Moshi.Builder()
                .build()
    }
}