package com.example.a19840422_erik_gutierrez_nyc_schools.di
import com.example.a19840422_erik_gutierrez_nyc_schools.api.NYCBaseUrl
import com.example.a19840422_erik_gutierrez_nyc_schools.api.NYCClient
import com.example.a19840422_erik_gutierrez_nyc_schools.api.NYCSchoolsApi
import com.example.a19840422_erik_gutierrez_nyc_schools.util.SerializedNulls
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NYCSchoolsModule {
    private const val NYC_BASE_URL = "https://data.cityofnewyork.us/resource/"

    @Provides
    @Singleton
    internal fun provideNYCApi(
            @NYCBaseUrl baseUrl: HttpUrl,
            @NYCClient okHttpClient: OkHttpClient
    ): NYCSchoolsApi {
        return baseRetrofit()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build()
                .create(NYCSchoolsApi::class.java)
    }

    @Provides
    @Singleton
    @NYCBaseUrl
    fun provideNYCBaseUrl(): HttpUrl {
        return NYC_BASE_URL.toHttpUrl()
    }

    @Provides
    @Singleton
    @NYCClient
    internal fun provideNYCHttpClient(
            loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor { chain ->
                    chain.request().newBuilder()
                            .build()
                            .let { chain.proceed(it) }
                }
                .addInterceptor(loggingInterceptor)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideMoshi(): Moshi {
        return Moshi.Builder()
                .add(SerializedNulls.Factory)
                .build()
    }

    internal fun baseRetrofit(): Retrofit.Builder {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(provideMoshi()))
    }
}