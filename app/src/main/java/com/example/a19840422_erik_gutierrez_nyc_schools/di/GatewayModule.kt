package com.example.a19840422_erik_gutierrez_nyc_schools.di

import com.example.a19840422_erik_gutierrez_nyc_schools.api.NYCMoshi
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object GatewayModule {

    @Provides
    @Singleton
    fun loggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    @NYCMoshi
    fun provideNYCMoshi(): Moshi {
        return MoshiModule.productsMoshi()
    }

}