package com.example.a19840422_erik_gutierrez_nyc_schools.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.a19840422_erik_gutierrez_nyc_schools.databinding.ActivityHighSchoolListBinding
import com.example.a19840422_erik_gutierrez_nyc_schools.view.listitem.HighSchoolListItem
import com.example.a19840422_erik_gutierrez_nyc_schools.viewmodel.HighSchoolsViewModel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Section
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class HighSchoolListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHighSchoolListBinding

    private val viewModel: HighSchoolsViewModel by viewModels()

    private var section = Section()
    private var groupAdapter = GroupAdapter<GroupieViewHolder>().apply {
        add(section)
        setHasStableIds(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHighSchoolListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.schoolsList.apply {
            adapter = groupAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        viewModel.schoolsList.observe(this, { schoolsList ->
            schoolsList.map { school ->
                HighSchoolListItem(
                    nycSchoolsResponse = school,
                    onItemClicked = { _, schoolDbn, overView ->
                        startActivity(
                            HighSchoolDetailActivity.newIntent(
                                context = this@HighSchoolListActivity,
                                dbn = schoolDbn,
                                schoolOverview = overView
                            )
                        )
                    }
                )
            }.also {
                section.update(it)
            }
        })

        viewModel.events.observe(this, { event ->
            when (event) {
                is HighSchoolsViewModel.Event.ProgressLoadingEvent -> {
                    binding.progressCircular.isVisible = event.isLoading
                }
                is HighSchoolsViewModel.Event.FailEvent -> {
                    Toast.makeText(this, "Oops! something went wrong..", Toast.LENGTH_SHORT).show()
                }
                is HighSchoolsViewModel.Event.ExceptionEvent -> {
                    Toast.makeText(this, event.exception, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context) = Intent(context, HighSchoolListActivity::class.java)
    }
}