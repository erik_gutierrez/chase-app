package com.example.a19840422_erik_gutierrez_nyc_schools.view.listitem

import android.view.View
import com.example.a19840422_erik_gutierrez_nyc_schools.R
import com.example.a19840422_erik_gutierrez_nyc_schools.api.models.NYCSchoolResponse
import com.example.a19840422_erik_gutierrez_nyc_schools.databinding.HighschoolListItemBinding
import com.xwray.groupie.viewbinding.BindableItem

class HighSchoolListItem(
        private val nycSchoolsResponse: NYCSchoolResponse,
        private val onItemClicked: (HighSchoolListItem, String?, String?) -> Unit
) : BindableItem<HighschoolListItemBinding>() {
    override fun getLayout(): Int = R.layout.highschool_list_item

    override fun initializeViewBinding(view: View): HighschoolListItemBinding {
        return HighschoolListItemBinding.bind(view)
    }

    override fun bind(viewBinding: HighschoolListItemBinding, position: Int) {
        viewBinding.apply {
            nycSchoolsResponse.run {
                schoolNameTv.text = school_name
                schoolAddressTv.text = primary_address_line_1
                schoolEmailAddressTv.text = school_email
                schoolPhoneNumberTv.text = phone_number
                schoolWebsite.text = website
                schoolSelectedIcon.setOnClickListener {
                    onItemClicked(this@HighSchoolListItem, dbn, overviewParagraph)
                }
            }
        }
    }
}