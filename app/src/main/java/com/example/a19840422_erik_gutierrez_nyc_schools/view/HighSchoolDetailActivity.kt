package com.example.a19840422_erik_gutierrez_nyc_schools.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.example.a19840422_erik_gutierrez_nyc_schools.R
import com.example.a19840422_erik_gutierrez_nyc_schools.databinding.ActivityHighSchoolDetailBinding
import com.example.a19840422_erik_gutierrez_nyc_schools.viewmodel.HighSchoolDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HighSchoolDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHighSchoolDetailBinding

    private val viewModel: HighSchoolDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHighSchoolDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val dbn = intent.getStringExtra(DBN)
        dbn?.let {
            viewModel.fetchSATScores(it)
        }

        viewModel.schoolsList.observe(this, { schools ->
            binding.detailView.run {
                schoolAddressTv.isGone = true
                schoolEmailAddressTv.isGone = true
                schoolPhoneNumberTv.isGone = true
                schoolWebsite.isGone = true
                schoolSelectedIcon.isGone = true

                if (schools.isNotEmpty()) {
                    schools.first().run {
                        schoolNameTv.text = schoolName
                        numOfSatTakers.apply {
                            isVisible = true
                            text = String.format(resources.getString(R.string.test_takers), numOfSatTestTakers.orEmpty())
                        }
                        satReadingAvg.apply {
                            isVisible = true
                            text = String.format(resources.getString(R.string.reading_avg), satCriticalReadingAvgScore.orEmpty())
                        }
                        satMathAvg.apply {
                            isVisible = true
                            text = String.format(resources.getString(R.string.math_avg), satMathAvgScore.orEmpty())
                        }
                        satWritingAvg.apply {
                            isVisible = true
                            text = String.format(resources.getString(R.string.writing_avg), satWritingAvgScore.orEmpty())
                        }
                        schoolOverview.apply {
                            isVisible = true
                            text = intent.getStringExtra(SCHOOL_OVERVIEW).orEmpty()
                        }
                    }
                } else {
                    schoolNameTv.text = resources.getString(R.string.no_data)
                    schoolImage.isGone = true
                }
            }
        })

        viewModel.events.observe(this, { event ->
            when (event) {
                is HighSchoolDetailViewModel.Event.ProgressLoadingEvent -> {
                    binding.progressCircular.isVisible = event.isLoading
                }
                is HighSchoolDetailViewModel.Event.FailEvent -> {
                    Toast.makeText(this, "Oops! something went wrong..", Toast.LENGTH_SHORT).show()
                }
                is HighSchoolDetailViewModel.Event.ExceptionEvent -> {
                    Toast.makeText(this, event.exception, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    companion object {
        const val DBN = "DBN"
        const val SCHOOL_OVERVIEW ="SCHOOL_OVERVIEW"

        @JvmStatic
        fun newIntent(context: Context, dbn: String?, schoolOverview: String?): Intent {
            val intent = Intent(context, HighSchoolDetailActivity::class.java)
            intent.putExtra(DBN, dbn.orEmpty())
            intent.putExtra(SCHOOL_OVERVIEW, schoolOverview.orEmpty())
            return intent
        }

    }
}