package com.example.a19840422_erik_gutierrez_nyc_schools.api

import com.example.a19840422_erik_gutierrez_nyc_schools.api.models.NYCSATScoresResponse
import com.example.a19840422_erik_gutierrez_nyc_schools.api.models.NYCSchoolResponse
import com.example.a19840422_erik_gutierrez_nyc_schools.util.ApiHeaders
import com.example.a19840422_erik_gutierrez_nyc_schools.util.SAT_SCORES
import com.example.a19840422_erik_gutierrez_nyc_schools.util.SCHOOLS
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

internal interface NYCSchoolsApi {

    @Headers(ApiHeaders.ACCEPT_JSON)
    @GET(SCHOOLS)
    fun getNYCSchools(): Observable<Response<List<NYCSchoolResponse>>>

    @Headers(ApiHeaders.ACCEPT_JSON)
    @GET(SAT_SCORES)
    fun getSATScores(
            @Query("dbn") dbn: String
    ) : Observable<Response<List<NYCSATScoresResponse>>>

}