package com.example.a19840422_erik_gutierrez_nyc_schools.api

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class NYCBaseUrl