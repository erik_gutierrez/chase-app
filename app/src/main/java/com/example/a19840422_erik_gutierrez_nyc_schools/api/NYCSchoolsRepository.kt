package com.example.a19840422_erik_gutierrez_nyc_schools.api

import com.example.a19840422_erik_gutierrez_nyc_schools.api.models.NYCSATScoresResponse
import com.example.a19840422_erik_gutierrez_nyc_schools.api.models.NYCSchoolResponse
import com.example.a19840422_erik_gutierrez_nyc_schools.util.Resource
import dagger.Reusable
import io.reactivex.Observable
import retrofit2.HttpException
import retrofit2.Response
import javax.inject.Inject

@Reusable
class NYCSchoolsRepository @Inject internal constructor(
   private val nycSchoolsApi: NYCSchoolsApi
) {

    fun getNYCSchools(): Observable<Resource<List<NYCSchoolResponse>>> =
        nycSchoolsApi.getNYCSchools().map { response -> response.toResource { it } }

    fun getSATScores(dbn: String) : Observable<Resource<List<NYCSATScoresResponse>>> =
        nycSchoolsApi.getSATScores(dbn = dbn).map { response -> response.toResource { it } }

    private fun <T, R> Response<T>.toResource(
        dataOnError: R? = null,
        transformer: (T?) -> R?
    ): Resource<R> {
        return Resource.from(
            dataProvider = { transformer(bodyOrError()) },
            fallbackData = dataOnError
        )
    }

    private fun <T> Response<T>.bodyOrError(): T? {
        return if (isSuccessful) body() else throw HttpException(this)
    }
}