package com.example.a19840422_erik_gutierrez_nyc_schools.api.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class NYCSATScoresResponse(

	@Json(name="dbn")
	val dbn: String? = null,

	@Json(name="sat_writing_avg_score")
	val satWritingAvgScore: String? = null,

	@Json(name="sat_critical_reading_avg_score")
	val satCriticalReadingAvgScore: String? = null,

	@Json(name="sat_math_avg_score")
	val satMathAvgScore: String? = null,

	@Json(name="school_name")
	val schoolName: String? = null,

	@Json(name="num_of_sat_test_takers")
	val numOfSatTestTakers: String? = null
) : Parcelable
