package com.example.a19840422_erik_gutierrez_nyc_schools.api.models

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class NYCSchoolResponse(

		@Json(name="bus")
		val bus: String? = null,

		@Json(name="grade9gefilledflag1")
		val grade9gefilledflag1: String? = null,

		@Json(name="method1")
		val method1: String? = null,

		@Json(name="bin")
		val bin: String? = null,

		@Json(name="admissionspriority21")
		val admissionspriority21: String? = null,

		@Json(name="borough")
		val borough: String? = null,

		@Json(name="grade9geapplicantsperseat1")
		val grade9geapplicantsperseat1: String? = null,

		@Json(name="ell_programs")
		val ellPrograms: String? = null,

		@Json(name="academicopportunities1")
		val academicopportunities1: String? = null,

		@Json(name="academicopportunities2")
		val academicopportunities2: String? = null,

		@Json(name="total_students")
		val totalStudents: String? = null,

		@Json(name="longitude")
		val longitude: String? = null,

		@Json(name="zip")
		val zip: String? = null,

		@Json(name="finalgrades")
		val finalgrades: String? = null,

		@Json(name="admissionspriority11")
		val admissionspriority11: String? = null,

		@Json(name="seats9swd1")
		val seats9swd1: String? = null,

		@Json(name="fax_number")
		val faxNumber: String? = null,

		@Json(name="school_sports")
		val schoolSports: String? = null,

		@Json(name="bbl")
		val bbl: String? = null,

		@Json(name="offer_rate1")
		val offerRate1: String? = null,

		@Json(name="dbn")
		val dbn: String? = null,

		@Json(name="pct_stu_safe")
		val pctStuSafe: String? = null,

		@Json(name="pct_stu_enough_variety")
		val pctStuEnoughVariety: String? = null,

		@Json(name="interest1")
		val interest1: String? = null,

		@Json(name="overview_paragraph")
		val overviewParagraph: String? = null,

		@Json(name="school_accessibility_description")
		val schoolAccessibilityDescription: String? = null,

		@Json(name="community_board")
		val communityBoard: String? = null,

		@Json(name="requirement2_1")
		val requirement21: String? = null,

		@Json(name="phone_number")
		val phone_number: String? = null,

		@Json(name="neighborhood")
		val neighborhood: String? = null,

		@Json(name="requirement4_1")
		val requirement41: String? = null,

		@Json(name="grade9swdfilledflag1")
		val grade9swdfilledflag1: String? = null,

		@Json(name="state_code")
		val stateCode: String? = null,

		@Json(name="council_district")
		val councilDistrict: String? = null,

		@Json(name="grade9geapplicants1")
		val grade9geapplicants1: String? = null,

		@Json(name="seats101")
		val seats101: String? = null,

		@Json(name="grade9swdapplicantsperseat1")
		val grade9swdapplicantsperseat1: String? = null,

		@Json(name="code1")
		val code1: String? = null,

		@Json(name="city")
		val city: String? = null,

		@Json(name="latitude")
		val latitude: String? = null,

		@Json(name="building_code")
		val buildingCode: String? = null,

		@Json(name="nta")
		val nta: String? = null,

		@Json(name="extracurricular_activities")
		val extracurricularActivities: String? = null,

		@Json(name="census_tract")
		val censusTract: String? = null,

		@Json(name="directions1")
		val directions1: String? = null,

		@Json(name="website")
		val website: String? = null,

		@Json(name="boro")
		val boro: String? = null,

		@Json(name="admissionspriority31")
		val admissionspriority31: String? = null,

		@Json(name="school_name")
		val school_name: String? = null,

		@Json(name="grades2018")
		val grades2018: String? = null,

		@Json(name="primary_address_line_1")
		val primary_address_line_1: String? = null,

		@Json(name="school_email")
		val school_email: String? = null,

		@Json(name="subway")
		val subway: String? = null,

		@Json(name="school_10th_seats")
		val school10thSeats: String? = null,

		@Json(name="grade9swdapplicants1")
		val grade9swdapplicants1: String? = null,

		@Json(name="attendance_rate")
		val attendanceRate: String? = null,

		@Json(name="requirement1_1")
		val requirement11: String? = null,

		@Json(name="program1")
		val program1: String? = null,

		@Json(name="location")
		val location: String? = null,

		@Json(name="requirement3_1")
		val requirement31: String? = null,

		@Json(name="seats9ge1")
		val seats9ge1: String? = null,

		@Json(name="requirement5_1")
		val requirement51: String? = null
) : Parcelable