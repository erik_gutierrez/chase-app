package com.example.a19840422_erik_gutierrez_nyc_schools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application() {
}