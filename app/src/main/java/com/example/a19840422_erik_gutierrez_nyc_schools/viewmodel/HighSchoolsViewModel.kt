package com.example.a19840422_erik_gutierrez_nyc_schools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import com.example.a19840422_erik_gutierrez_nyc_schools.api.NYCSchoolsRepository
import com.example.a19840422_erik_gutierrez_nyc_schools.api.models.NYCSchoolResponse
import com.example.a19840422_erik_gutierrez_nyc_schools.util.Resource
import com.example.a19840422_erik_gutierrez_nyc_schools.util.disposedBy
import com.hadilq.liveevent.LiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class HighSchoolsViewModel @Inject constructor(
        repository: NYCSchoolsRepository
) : ViewModel() {

    private val _event = LiveEvent<Event>()
    val events: LiveData<Event> get() = _event

    private val _schoolsList: MutableLiveData<List<NYCSchoolResponse>> = MutableLiveData()
    val schoolsList: LiveData<List<NYCSchoolResponse>> get() = _schoolsList.distinctUntilChanged()

    private val disposables = CompositeDisposable()

    init {
        _event.postValue(Event.ProgressLoadingEvent(isLoading = true))
        repository.getNYCSchools()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ results ->
                _event.postValue(Event.ProgressLoadingEvent(false))

                when (results) {
                    is Resource.Success -> onSuccessResponse(results.data)
                    is Resource.Failure -> {
                        _event.postValue(Event.FailEvent)
                        Timber.e(results.error.toString(), "Error")
                    }
                    else -> {
                        //no-op
                    }
                }
            }, { _event.postValue(Event.ExceptionEvent(it.message.orEmpty())) })
            .disposedBy(disposables)
    }

    private fun onSuccessResponse(response: List<NYCSchoolResponse>?) {
        _event.postValue(Event.ProgressLoadingEvent(isLoading = false))
        if (response != null) {
            _schoolsList.postValue(response)
        } else {
            _event.postValue(Event.FailEvent)
        }
    }

    sealed class Event {
        object FailEvent : Event()
        data class ExceptionEvent(val exception: String) : Event()
        data class ProgressLoadingEvent(val isLoading: Boolean = false) : Event()
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

}
