import Versions.coroutinesVersion
import Versions.groupieVersion
import Versions.hiltVersion
import Versions.kotlinVersion
import Versions.lifecycleVersion
import Versions.moshiVersion
import Versions.navigation
import Versions.okHttpVersion
import Versions.retrofitVersion
import Versions.roomVersion

object Versions {
    const val kotlinVersion = "1.4.32"
    const val hiltVersion = "2.33-beta"
    const val lifecycleVersion = "2.3.1"
    const val groupieVersion = "2.9.0"
    const val coroutinesVersion = "1.4.2"
    const val moshiVersion = "1.11.0"
    const val retrofitVersion = "2.9.0"
    const val okHttpVersion = "4.9.0"
    const val roomVersion = "2.2.6"
    const val navigation = "2.3.0"
}

object Deps {


    const val coreKtx = "androidx.core:core-ktx:1.3.2"
    const val appCompat = "androidx.appcompat:appcompat:1.2.0"
    const val material = "com.google.android.material:material:1.3.0"

    const val runtimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion"
    const val java8 = "androidx.lifecycle:lifecycle-common-java8:$lifecycleVersion"
    const val livedataCoreKtx = "androidx.lifecycle:lifecycle-livedata-core-ktx:$lifecycleVersion"
    const val livedataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion"
    const val coreTesting = "androidx.arch.core:core-testing:2.1.0"
    const val reactiveStreams = "androidx.lifecycle:lifecycle-reactivestreams-ktx:$lifecycleVersion"
    const val viewmodelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion"
    const val customView = "androidx.customview:customview:1.1.0"
    const val uiLayout = "androidx.ui:ui-layout:0.1.0-dev13"
    const val legacySupport = "androidx.legacy:legacy-support-v4:1.0.0"

    const val jUnit = "junit:junit:4.13.2"
    const val jUnitExt = "androidx.test.ext:junit:1.1.2"
    const val espressoCore = "androidx.test.espresso:espresso-core:3.3.0"

    const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.4"
    const val cardview = "androidx.cardview:cardview:1.0.0"

    const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion"
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion"
    const val jdk7 = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    const val jdk8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion"

    const val liveEvent = "com.github.hadilq.liveevent:liveevent:1.2.0"

    const val hilt = "com.google.dagger:hilt-android:$hiltVersion"
    const val hiltCompiler = "com.google.dagger:hilt-android-compiler:$hiltVersion"

    const val groupie = "com.xwray:groupie:$groupieVersion"
    const val groupieViewBinding = "com.xwray:groupie-viewbinding:$groupieVersion"
    const val groupieExt = "com.xwray:groupie-kotlin-android-extensions:$groupieVersion"

    const val moshi = "com.squareup.moshi:moshi:$moshiVersion"
    const val moshiKotlin = "com.squareup.moshi:moshi-kotlin:$moshiVersion"
    const val moshiCodegen = "com.squareup.moshi:moshi-kotlin-codegen:$moshiVersion"

    const val picasso = "com.squareup.picasso:picasso:2.71828"
    const val picassoDownloader = "com.jakewharton.picasso:picasso2-okhttp3-downloader:1.1.0"

    const val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
    const val retrofitGson = "com.squareup.retrofit2:converter-gson:$retrofitVersion"
    const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:$retrofitVersion"
    const val retrofitRxJava = "com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion"

    const val okHttp = "com.squareup.okhttp3:okhttp:$okHttpVersion"
    const val okHttpInterceptor = "com.squareup.okhttp3:logging-interceptor:$okHttpVersion"
    const val okHttpUrlConnection = "com.squareup.okhttp3:okhttp-urlconnection:$okHttpVersion"
    const val okHttpMockWebBrowser = "com.squareup.okhttp3:mockwebserver:$okHttpVersion"

    const val rxJava = "io.reactivex.rxjava2:rxjava:2.2.20"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:2.1.1"
    const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:2.4.0"
    const val rxRelay = "com.jakewharton.rxrelay2:rxrelay:2.1.1"

    const val room = "androidx.room:room-runtime:$roomVersion"
    const val roomCompiler = "androidx.room:room-compiler:$roomVersion"
    const val roomRxJava = "androidx.room:room-rxjava2:$roomVersion"
    const val roomTesting = "androidx.room:room-testing:$roomVersion"

    const val gson = "com.google.code.gson:gson:2.8.6"
    const val timber = "com.jakewharton.timber:timber:4.7.1"

    const val config = "com.google.firebase:firebase-config-ktx:20.0.2"
    const val core = "com.google.firebase:firebase-core:17.5.1"
    const val dynamicLinks = "com.google.firebase:firebase-dynamic-links-ktx:19.1.1"
    const val messaging = "com.google.firebase:firebase-messaging-ktx:21.0.1"
    const val crashlytics = "com.google.firebase:firebase-crashlytics-ktx:17.3.0"

    const val rxBindingKotlin = "com.jakewharton.rxbinding2:rxbinding-kotlin:2.1.1" // https://github.com/JakeWharton/RxBinding
    const val rxBindingAppCompat = "com.jakewharton.rxbinding2:rxbinding-appcompat-v7-kotlin:2.1.1"
    const val rxBindingRecyclerview = "com.jakewharton.rxbinding2:rxbinding-recyclerview-v7-kotlin:2.1.1"
    const val rxBindingDesign = "com.jakewharton.rxbinding2:rxbinding-design-kotlin:2.1.1"
    const val rxBindingSupport = "com.jakewharton.rxbinding2:rxbinding-support-v4-kotlin:2.1.1"

    const val navFragment= "androidx.navigation:navigation-fragment-ktx:$navigation"
    const val navKtx = "androidx.navigation:navigation-ui-ktx:$navigation"
}